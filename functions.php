<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',                               // Scripts and stylesheets
  'lib/extras.php',                               // Custom functions
  'lib/setup.php',                                // Theme setup
  'lib/titles.php',                               // Page titles
  'lib/wrapper.php',                              // Theme wrapper class
  'lib/customizer.php',                           // Theme customizer
  'lib/vendor/dimox_breadcrumbs.php',             // Breadcrumb
  'lib/vendor/wp_bootstrap_navwalker.php',        // WP Bootstrap Navwalker
  'lib/custom/pp-whitelabel.php',              // White label functions  
  'lib/custom/pp-vc-extend.php',                   // Extend Visual Composer
  'lib/custom/theme-options/redux-framework.php', // Theme Options framework
  'lib/custom/theme-options/configuration.php',   // Theme Options config  
  'shortcodes/shortcodes-init.php',               // Register shortcodes (incl VC Mapping)
  'posttypes/posttypes-init.php'                 // Register custom post types
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

if ( function_exists( 'the_champ_init' ) ) {
  remove_action( 'admin_init', 'the_champ_options_init' );
}

add_filter( 'user_contactmethods', 'extend_user_contactmethods' );
function extend_user_contactmethods( $user_contactmethods ) {

  $instagram_icon = '<div style="position:relative"><img style="position:absolute;top:0.24em;right:-26.5em;" src="' . get_template_directory_uri() . '/dist/images/admin/social/instagram.png" alt="Instagram" height="16" />';
  $user_contactmethods['pinkpanda_instagram_profile'] = $instagram_icon . __( 'Instagram Profile URL', 'seopressor' ) . '</div>';

  $linkedin_icon = '<div style="position:relative"><img style="position:absolute;top:0.24em;right:-26.5em;" src="' . get_template_directory_uri() . '/dist/images/admin/social/linkedin.png" alt="LinkedIn" height="16" />'; 

  $user_contactmethods['pinkpanda_linkedin_profile'] = $linkedin_icon . __( 'LinkedIn Profile URL', 'seopressor' ) . '</div>'; 


  return $user_contactmethods;
}

add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

function add_async_attribute($tag, $handle) {
    if ( 'google-recaptcha' !== $handle )
        return $tag;
    return str_replace( ' src', ' async="async" src', $tag );
}