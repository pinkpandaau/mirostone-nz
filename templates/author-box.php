<?php

	global $post;

    $author_id = $post->post_author;

	$user_email = get_the_author_meta( 'user_email' );
	$user_bio = get_the_author_meta( 'description' );

	$user_avatar = get_avatar_url( get_the_author_meta( 'ID' ) );

	$user_nickname = get_the_author_meta( 'display_name', $author_id );
	$user_firstname = get_the_author_meta('first_name');
	$user_lastname = get_the_author_meta('last_name');

	if (!$user_firstname) {
		$user_fullname = $user_nickname;
	} else {
		$user_fullname = $user_firstname . ' ' . $user_lastname;
	}

	$user_posts_url = get_author_posts_url( $author_id );

	$user_website = get_the_author_meta( 'user_url', $author_id );

	$facebook_url = get_the_author_meta( 'seopressor_facebook_og_author', $author_id );
	$twitter_handle = get_the_author_meta( 'seopressor_twitter_user_card', $author_id );
	

	$google_url = trim(get_the_author_meta( 'seopressor_google_plus_auth_url', $author_id ));

	$instagram_url = get_the_author_meta( 'pinkpanda_instagram_profile', $author_id );
	$linkedin_url = get_the_author_meta( 'pinkpanda_linkedin_profile', $author_id );


?>
<div class="container-fluid author-wrapper">

	<div class="col-sm-2 col-xs-12">

		<div class="author-profile">

			<img id="author-avatar" src="<?php echo $user_avatar; ?>" />

		</div><!-- end author-profile -->

	</div>

	<div class="col-sm-10 col-xs-12">

		<div class="author-content">

			<!-- NAME -->
			<a href="<?php echo $user_posts_url; ?>">
				<h4><span>By </span><?php echo $user_fullname; ?></h4>
			</a>

			<!-- BIO -->
			<div class="vcard author">
				<span class="fn">
					<?php echo $user_bio; ?>
				</span>
			</div><!-- end vcard author -->

			<!-- SOCIAL MEDIA -->
			<?php if ($facebook_url || $twitter_handle || $instagram_url || $google_url || $linkedin_url) { ?>

				<div class="author-social">

					<span class="follow-text">Connect with me</span>
					<br>

					<?php if ($facebook_url) { ?>
						<a class="facebook" href="<?php echo $facebook_url; ?>" target="_blank">
							<i class="fa fa-facebook"></i>
						</a>
					<?php } ?>

					<?php if ($twitter_handle) { ?>
						<?php $twitter_handle = str_replace("@", "", $twitter_handle); ?>
						<a class="twitter" href="https://twitter.com/<?php echo $twitter_handle; ?>" target="_blank" >
							<i class="fa fa-twitter"></i>
						</a>
					<?php } ?>

					<?php if ($instagram_url) { ?>
						<a class="instagram" href="<?php echo $instagram_url; ?>" target="_blank">
							<i class="fa fa-instagram"></i>
						</a>
					<?php } ?>

					<?php if ($google_url) { ?>
						<a class="googleplus" href="<?php echo $google_url; ?>" target="_blank">
							<i class="fa fa-google-plus"></i>
						</a>
					<?php } ?>

					<?php if ($linkedin_url) { ?>
						<a class="linkedin" href="<?php echo $linkedin_url; ?>" target="_blank">
							<i class="fa fa-linkedin"></i>
						</a>
					<?php } ?>


				</div><!-- end author-social -->

			<?php } ?>


		</div><!-- end author-content -->

	</div>

</div><!-- end container-fluid -->