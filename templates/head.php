<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-RHEERMHSY3"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-RHEERMHSY3');
	</script>
	
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">
	<?php wp_head(); ?>	
	<meta name="google-site-verification" content="DxLfxV8WxsqDD-jirXJwQdc8kfua0nKBwpFh-KVDWTU" />
	<meta name="google-site-verification" content="RH9iv9b9paZATg_FTPYc4ztgx1Bm-BHmgmgVJrvT0Hw" />
</head>
