<?php global $pp_options; ?>

<header id="masthead" class="site-header" role="banner">
    <!-- topbar start -->
    <div class="container-fluid topbar">
        <div class="container">
            <div class="row">
                <div class="topbar-email col-md-6 col-sm-8 text-left">
                    <?php if($pp_options['contact-info-email'] != null) { ?>
                        <p>For more information, please contact us at 
                            <a href="mailto:<?php echo $pp_options['contact-info-email']; ?>">
                                <?php echo $pp_options['contact-info-email']; ?></a>
                        </p>					
                    <?php } ?>
                </div>
                <div class="topbar-insta col-md-6 col-sm-4 text-right">
                    <span class="instagram">            
                        <?php if($pp_options['social-media-instagram'] != null) { ?>
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                            <a href="http://www.instagram.com/<?php echo $pp_options['social-media-instagram']; ?>" target="_blank">
                                @<?php echo $pp_options['social-media-instagram']; ?>
                            </a>
                        <?php } ?>
                    </span>
                </div>
            </div>      
        </div>      
    </div>
  
  <div class="container">
    <?php include ("header/header-v1.php"); ?>
    <?php //include ("header/header-mobile.php"); ?>
  </div>

</header>
