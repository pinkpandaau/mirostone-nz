<?php global $pp_options; ?>

<h3 id="brochure_toggle" class="active">Download Brochure <i class="fa fa-arrow-circle-right"></i></h3>
<div class="primary_popup_form pdf-close">
	<div class="pdf-close-button"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
	<?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?><!-- PDF FORM -->
</div>

<script>
	jQuery(document).ready(function() {

		setTimeout(function(){ 
			jQuery('.primary_popup_form').removeClass('pdf-close');
			jQuery('#brochure_toggle').removeClass('active');
		 }, 5000);

		jQuery('.primary_popup_form .pdf-close-button').click(function() {
			jQuery('.primary_popup_form').addClass('pdf-close triggered');
			jQuery('.primary_popup_form').removeClass('triggered');
			jQuery('.popup_overlay').removeClass('active');
			jQuery('#brochure_toggle').addClass('active');
		});

		jQuery('#mobile_brochure').click(function() {
			jQuery('.primary_popup_form').removeClass('pdf-close');
			jQuery('.primary_popup_form').addClass('triggered');
			jQuery('.popup_overlay').addClass('active');
		});

		jQuery('#brochure_toggle').click(function() {
			jQuery(this).removeClass('active');
			jQuery('.primary_popup_form').removeClass('pdf-close');
			jQuery('.primary_popup_form').addClass('triggered');
		});

		jQuery('.popup_overlay').click(function() {
			jQuery('.primary_popup_form').addClass('pdf-close triggered');
			jQuery('.popup_overlay').removeClass('active');
			jQuery('#brochure_toggle').addClass('active');
		});
		
		jQuery('.primary_popup_form #gform_submit_button_').click(function() {
			jQuery(this).text('Submitting');
		})

		jQuery(document).on("gform_confirmation_loaded", function (e, form_id) {
			jQuery('.primary_popup_form #gform_submit_button_').text('Submitted');
		});

		jQuery(document).on("gform_validation_message", function (e, form_id) {
			jQuery('.primary_popup_form #gform_submit_button_').text('Submit');
		});
	});
</script>



<footer class="ms-footer">

	<div class="container">

		<div class="row">

			<div class="col-sm-3 footer-first">
				<img class="img-responsive pull-right" src="<?php echo get_template_directory_uri(); ?>/dist/images/logo/mirostone-logo-white.png" alt="Mirostone - Solid Surface Benchtops" />
			</div>

			<div class="col-sm-3 footer-second">
					<?php
						if (has_nav_menu('primary_navigation')) :
								wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'footer-menu']);
						endif;
					?>				

			</div><!-- end second -->

			<div class="col-sm-3 footer-third">

				<div class="footer-contact text-left">
					
					<p>
						<strong>Head Office</strong><br>
						<?php if($pp_options['contact-info-street-address'] != null && $pp_options['contact-info-suburb'] != null && $pp_options['contact-info-state'] != null && $pp_options['contact-info-postcode'] != null ) { ?>
							<?php echo $pp_options['contact-info-street-address']; ?>,
							<?php echo $pp_options['contact-info-suburb']; ?><br>
							<?php echo $pp_options['contact-info-state']; ?>
							<?php echo $pp_options['contact-info-postcode']; ?>
						<?php } ?>
					</p>
					
				</div>

			</div><!-- end third -->

			<div class="col-sm-3 footer-fourth">
					<p>
						<?php if($pp_options['contact-info-phone'] != null) { ?>
							Telephone:
							<strong><a href="tel:<?php echo preg_replace('/\s+/', '', $pp_options['contact-info-phone']); ?>">
								<?php echo $pp_options['contact-info-phone']; ?>
							</a></strong>
						<?php } ?>

						<?php if($pp_options['contact-info-email'] != null) { ?>
							<a href="mailto:<?php echo $pp_options['contact-info-email']; ?>" class="footer-email">
								<?php echo $pp_options['contact-info-email']; ?>
							</a>
						<?php } ?>
					</p>
					<p><a class="btn btn_footer" target="_blank" href="http://www.amorini.co.nz/">www.amorini.co.nz</a></p>
					
			</div><!-- end fourth -->

		</div><!-- end row -->

	</div><!-- end container -->

	<div class="sub-footer">
		<div class="container">Copyright © Amorini - All Rights Reserved</div>
	</div><!-- end sub-footer -->

	<a id="slide-top" class="img-circle">Go to top</a><!-- end slide-top -->

	<a id="amorini-toggle" class="toggle-amorini">
		<img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo/amorini-logo-vert.png" width="36" height="120" alt="About Amorini" class="visible-lg visible-md">
		<img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo/amorini-icon.png" width="24" height="24" alt="About Amorini" class="visible-xs visible-sm">
	</a><!-- end amorini-toggle -->

	<section id="amorini-data">
      <div class="inner">
        <p class="text-right">
			<img id="slide-close" class="toggle-amorini" src="<?php echo get_template_directory_uri(); ?>/dist/images/icon_arrow_featured_left.png" alt="Close" width="22" height="16">
		</p>
		<h2><img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo/amorini-logo.png" alt="Amorini" width="156" height="46"></h2>
		<p><span>Visit our website to keep up to date with all the latest Amorini Products news.</span></p>
		<p><a class="btn btn-primary btn-amorini" href="http://www.amorini.com.au/" target="_blank">Visit Website</a></p>
      </div>
    </section><!-- end amorini-data -->

	<div id="amorini-overlay" class="toggle-amorini"></div><!-- end amorini-overlay -->

</footer><!-- end ms-footer -->