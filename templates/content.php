<article <?php post_class(); ?>>

	<div class="inside">

		<div class="col-date col-md-1 hidden-sm hidden-xs">

			<span class="month"><?php the_time('M'); ?></span>
			<span class="day"><?php the_time('d'); ?></span>

		</div><!-- end col-date -->

		<div class="col-info col-md-11">

			<div class="intro">

				<div class="post-image">

					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('thumbnail', array(
						  'class' => 'gat-post-thumb img-responsive'
						)); ?>
					</a>
					
				</div><!-- end post-image -->

				<div class="post-info">

					<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

					<time class="updated hidden-lg hidden-md" datetime="<?= get_post_time('c', true); ?>">
						<?= get_the_date(); ?><br>
					</time>
			    	<?php //get_template_part('templates/entry-meta'); ?>

					<span class="meta-author">
						<span><?php echo __('By', 'pink-panda'); ?></span>
						<?php the_author_posts_link(); ?>
					</span>

					<span class="meta-category">| <?php the_category(', '); ?></span>
					<span class="meta-comment-count">
						| <a href="<?php comments_link(); ?>">
							<?php comments_number( __('No Comments', 'pink-panda'), __('One Comment ', 'pink-panda'), __('% Comments', 'pink-panda') ); ?>
								
						</a>
					</span>

					<div class="entry-summary visible-sm visible-xs">
						<?php the_excerpt(); ?>
					</div>

				</div><!-- end post-info -->

			</div><!-- end intro -->

			

			<div class="entry-summary hidden-sm hidden-xs">
				<?php the_excerpt(); ?>
			</div>

		</div><!-- end col-info -->

	</div><!-- end inside -->


</article>
