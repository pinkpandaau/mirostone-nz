<?php 

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>

    <header class="row text-center <?php if (!(has_post_thumbnail()) ) {echo 'no-image'; } ?>" style="background-image: url('<?php echo the_post_thumbnail_url(); ?>');" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;"> 
      <div class="header-overlay">
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <?php get_template_part('templates/entry-meta'); ?>
      </div>
    </header>

    <div class="container">
      <div class="post-content col-md-8">
          <div class="entry-content">
            <?php the_content(); ?>
          </div>
          <div class="author-box">
            <?php get_template_part('templates/author-box'); ?>
          </div><!-- end author-box -->
          <footer>
            <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
          </footer>
          <?php comments_template('/templates/comments.php'); ?>
      </div>
      
      <?php if (Setup\display_sidebar()) : ?>
          <aside class="col-md-4 sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
      <?php endif; ?>

    </div>

    
  </article>
<?php endwhile; ?>
