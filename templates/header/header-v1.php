 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
 <nav class="nav-primary">        
    <div class="row">       
        <div class="nav-wrapper">
            <div class="col-md-4 col-sm-4 logo-outer">
                <div class="logo-wrapper col-twins">
                    <button class="navbar-toggler visible-xs" type="button" data-toggle="collapse" data-target="#navbar-header" aria-controls="navbar-header">
                        ☰
                    </button>
                    <a href="<?php echo get_site_url(); ?>">
                        <img src='<?php echo get_template_directory_uri(); ?>/dist/images/logo/mirostone-logo.png' alt="Mirostone - Solid Surface Benchtops" class="img-responsive" id="site-logo">
                    </a>
                </div><!--logo-wrapper end-->
            </div>

            <div class="main-menu navbar-collapse collapse col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-0 col-xs-10 col-xs-offset-1" id="navbar-header">
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav  col-twins']);
                endif;
                ?>
            </div><!--main-menu end-->
        </div>
    </div><!--row end-->
</nav><!--nav-primary end-->
