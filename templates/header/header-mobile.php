<nav class="mobile-nav visible-xs visible-sm">
  <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
    endif;
  ?>
</nav>
 
<div class="navbar navbar-fixed visible-xs visible-sm">
    <!--Include your brand here-->
    <a class="navbar-brand" href="<?php echo get_site_url(); ?>"><img src='<?php echo get_template_directory_uri(); ?>/dist/images/logo/mirostone-logo.png' alt="Mirostone" class="img-responsive" id="site-logo"></a>
    <div class="navbar-header pull-right">
      <button id="nav-expander" class="hamburger hamburger--squeeze nav-expander fixed" type="button">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
    </div>
</div>

<div class="overlay-shadow"></div>