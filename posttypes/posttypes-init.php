<?php

// ! File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

$shortcodes = [
  'pinkpanda/surfaces.php'
];

foreach ($shortcodes as $file) {
  $file = 'posttypes/' . $file;
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating CPT - ' . $file, 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);