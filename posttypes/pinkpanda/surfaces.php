<?php

// ! File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Register Post Type
function create_surfaces_post_type() {
	$surface_labels = array(
	    'name' => __('Surfaces', 'taxonomy general name', 'pink-panda'),
	    'singular_name' => __('Surface', 'pink-panda'),
	    'search_items' => __('Search Surfaces', 'pink-panda'),
	    'all_items' => __('Surfaces', 'pink-panda'),
	    'parent_item' => __('Parent Surface', 'pink-panda'),
	    'edit_item' => __('Edit Surface', 'pink-panda'),
	    'update_item' => __('Update Surface', 'pink-panda'),
	    'add_new_item' => __('Add New Surface', 'pink-panda'),
	    'not_found' => __('No Surface found', 'pink-panda')
	);

    $custom_slug = 'surfaces';

    $args = array(
        'labels' => $surface_labels,
        'rewrite' => array('slug' => $custom_slug, 'with_front' => false),
        'singular_label' => __('Surface', 'pink-panda'),
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'hierarchical' => false,
        'menu_position' => 8,
        'menu_icon' => 'dashicons-archive',
        //'supports' => array('title', 'thumbnail')
        'supports' => array('title', 'editor', 'thumbnail')
    );
    register_post_type('surface', $args);
    flush_rewrite_rules();
    register_taxonomy("surface_category", array("surface"), array("hierarchical" => true, "label" => __('Surface Categories', 'pink-panda'), 'query_var' => true, 'rewrite' => true));
}
add_action( 'init', 'create_surfaces_post_type' );