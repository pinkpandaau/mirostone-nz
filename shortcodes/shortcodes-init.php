<?php

// ! File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

$shortcodes = [
  'pinkpanda/pp-button.php',
  'pinkpanda/pp-slideshow.php',
  'pinkpanda/pp-downloadable.php',
  'pinkpanda/pp-service-note.php',
  'pinkpanda/pp-divider.php',
  'pinkpanda/pp-surfaces.php',
  'pinkpanda/pp-cta.php',
  'pinkpanda/pp-contactinfo.php'
];

if (function_exists('vc_map')) {

  foreach ($shortcodes as $file) {
    $file = 'shortcodes/' . $file;
    if (!$filepath = locate_template($file)) {
      trigger_error(sprintf(__('Error locating shortcode - ' . $file, 'sage'), $file), E_USER_ERROR);
    }

    require_once $filepath;
  }
  unset($file, $filepath);

}