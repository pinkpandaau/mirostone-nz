<?php

//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
    "name" => __("Service Notes", "pink-panda"),
    "base" => "gat_service_notes",
    "as_parent" => array('only' => 'single_note'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
    "is_container" => true,
    'icon' => vc_icon(),
    "params" => array(
        // add params same as with any other content element
        array(
            //"type" => "textfield",
            //"heading" => __("Extra class name", "pink-panda"),
            //"param_name" => "el_class",
            //"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pink-panda")
        )
    ),
    "js_view" => 'VcColumnView'
) );
vc_map( array(
    "name" => __("Service", "pink-panda"),
    "base" => "single_note",
    "content_element" => true,
    "as_child" => array('only' => 'gat_service_notes'), // Use only|except attributes to limit parent (separate multiple values with comma)
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Service Category", "pink-panda"),
            "param_name" => "service_note_category",
            "description" => __("Please enter the title of this service", "pink-panda"),
            "admin_label" => true
        ),
        array(
            "type" => "textarea_html",
            "heading" => __("Services Description", "pink-panda"),
            "param_name" => "content",
            "description" => __("Please enter the description of this service", "pink-panda")
        )
    )
) );
//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_gat_service_notes extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_single_note extends WPBakeryShortCode {
    }
}

// Container Shortcode
add_shortcode( 'gat_service_notes', 'gat_service_notes_shortcode' );
function gat_service_notes_shortcode($atts, $content = "") {

    ob_start();
    ?>

    <div class="service_notes row">

        <?php echo do_shortcode($content); ?>

    </div><!-- end service_notes -->

    <script>
    jQuery(function() {
        jQuery('.single_note').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    });
    </script>
    
    <?php $myvariable = ob_get_clean();
    return $myvariable;

}


add_shortcode( 'single_note', 'single_note_shortcode' );
function single_note_shortcode($atts, $content = "") {

    extract(shortcode_atts(array(
        'service_note_category' => '',
    ), $atts));

    $subtext = wpautop($content);
    
    ob_start();
    ?>

    <div class="col-md-4 single_note">

        <div class="boxed">
            <h2><?php echo $service_note_category; ?></h2>
            <p><?php echo $content; ?></p>
            <span class="bottom-line"></span>
        </div><!-- end boxed -->

    </div><!-- end col-md-4 -->
    
    <?php $myvariable = ob_get_clean();
    return $myvariable;
}


?>