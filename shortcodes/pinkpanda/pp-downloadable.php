<?php

// Pink Panda Button
add_shortcode( 'downloadable', 'downloadable_main' );
function downloadable_main($atts) {

	extract(shortcode_atts(array(
		'downloadable_pdf' => '',
		'downloadable_subtext' => ''
	), $atts));

	$href = vc_build_link( $downloadable_pdf );

	ob_start();

	?>

	<div class="downloadable">
		<p>
			<a href="<?php echo $href['url']; ?>"><?php echo $href['title']; ?></a>
			<?php if ($downloadable_subtext) { echo ' - ' . $downloadable_subtext; } ?>
		</p>
	</div><!-- end downloadable -->

	<?php $myvariable = ob_get_clean();
	return $myvariable;
}

add_action( 'vc_before_init', 'pp_vc_downloadable_main' );



function pp_vc_downloadable_main() {
	
	vc_map( array(
		'name' => __( 'Downloadable', 'pink-panda' ),
		'base' => 'downloadable',
		'icon' => vc_icon(),
		'category' => __( 'Mirostone', 'pink-panda' ),
		'description' => __( 'Downloadable links created by Pink Panda', 'pink-panda' ),
		'show_settings_on_create' => true,
		'params' => array(
			array(
	            'type' => 'vc_link',
	            'heading' => __( 'Downloadable PDF', 'pink-panda' ),
	            'param_name' => 'downloadable_pdf',
	            'value' => '',
	            'holder' => 'hidden',
				'admin_label' => true
	        ),
			array(
				'type' => 'textarea',
				'heading' => __( 'Downloadable Subtext', 'pink-panda' ),
				'param_name' => 'downloadable_subtext',
				'holder' => 'hidden',
				'admin_label' => false
			),

		),
	));
}