<?php

//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
    "name" => __("MS Slideshow", "pink-panda"),
    "base" => "ms_slideshow",
    "as_parent" => array('only' => 'ms_slide'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => false,
    'category' => __( 'Mirostone', 'pink-panda' ),
    "is_container" => true,
    'icon' => vc_icon(),
    "params" => array(
        // add params same as with any other content element
        array(
            //"type" => "textfield",
            //"heading" => __("Extra class name", "pink-panda"),
            //"param_name" => "el_class",
            //"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pink-panda")
        )
    ),
    "js_view" => 'VcColumnView'
) );
vc_map( array(
    "name" => __("Slide", "pink-panda"),
    "base" => "ms_slide",
    "content_element" => true,
    "as_child" => array('only' => 'ms_slideshow'), // Use only|except attributes to limit parent (separate multiple values with comma)
    "params" => array(
        // add params same as with any other content element
        array(
            'type' => 'attach_image',
            'heading' => __( 'Slide Image', 'pink-panda' ),
            'param_name' => 'slide_image',
            'value' => '',
        ),
        array(
            "type" => "textfield",
            "heading" => __("Heading", "pink-panda"),
            "param_name" => "slide_heading",
            "description" => __("Please enter a heading for your slide (optional)", "pink-panda"),
            "admin_label" => true
        ),
        array(
            "type" => "textfield",
            "heading" => __("Sub Text", "pink-panda"),
            "param_name" => "slide_sub",
            "description" => __("Please enter the some subtext (optional)", "pink-panda"),
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Text Colour', 'pink-panda' ),
            'param_name' => 'slide_colour',
            'value' => array(
                'Light Grey' => 'grey',
                'White' => 'white',
                'Black' => 'black'
            )
        ),
    )
) );
//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_ms_slideshow extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_ms_slide extends WPBakeryShortCode {
    }
}

// List Shortcode
add_shortcode( 'ms_slideshow', 'ms_slideshow_shortcode' );
function ms_slideshow_shortcode($atts, $content = "") {

	ob_start();
	?>

	<div id="homeSlide" class="ms_slideshow">

    	<?php echo do_shortcode($content); ?>

	</div><!-- end our_people -->

	<?php $myvariable = ob_get_clean();
	return $myvariable;

}


// Slide shortcode
add_shortcode( 'ms_slide', 'ms_slide_shortcode' );
function ms_slide_shortcode($atts, $content = "") {
	extract(shortcode_atts(array(
		'slide_image' => '',
		'slide_title' => '',
        'slide_heading' => '',
        'slide_sub' => '',
        'slide_colour' => ''
	), $atts));
	
	//$person_img = wp_get_attachment_image_src($person_image,'large');
	//$person_img_url = $person_img[0];

	//$title = sanitize_title($reg_name);
	//$content = wpautop($content);

	$slide_alt = get_post_meta($slide_image, '_wp_attachment_image_alt', true);
	$slide_title = get_the_title($slide_image);

	$slide_img = wp_get_attachment_image_src($slide_image,'full');
	$slide_img_url = $slide_img[0];

    $no_sub = '';

    if (!$slide_sub || $slide_sub == '') {
        $no_sub = 'no_subtext'; 
    }


	ob_start();
	?>

	<div class="ms_slide <?php echo $slide_colour; ?>" style="background-image: url('<?php echo $slide_img_url; ?>');" data-speed="2">
        
        <div class="overlay">
            <div class="container">
                <?php if ($slide_heading && $slide_heading != '') { ?>

                    <h1 class="<?php echo $no_sub; ?>"><?php echo $slide_heading; ?></h1>

                <?php } ?>

                <?php if ($slide_sub && $slide_sub != '' && $slide_heading && $slide_heading != '') { ?>

                    <h4><a href="#learn-more"><?php echo $slide_sub; ?></a></h4>

                <?php } ?>
            </div>
        </div>
        
	    
		<!--<img src="<?php echo $slide_img_url; ?>" alt="<?php echo $slide_alt; ?>" title="<?php echo $slide_title; ?>" />-->
	    <?php //echo $slide_title; ?>

    </div><!-- end ms_slide -->
	

	<?php $myvariable = ob_get_clean();
	return $myvariable;
}


?>