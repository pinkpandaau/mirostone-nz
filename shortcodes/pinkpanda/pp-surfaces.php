<?php

// Pink Panda Button
add_shortcode( 'pp_surfaces', 'pp_surfaces_main' );
function pp_surfaces_main($atts) {

	extract(shortcode_atts(array(
		'btn_title' => 'Read More',
		'btn_link' => '#',
		'is_elastic' => ''
	), $atts));

	ob_start();

	?>

	<div class="surfaces-wrapper">

		<ul class="ms_surfaces kwicks kwicks-horizontal clearfix hidden-xs">

	        	<?php
	            $args = array(
	                'post_type' => 'surface',
	                'posts_per_page' => -1,
	                //'meta_key' => 'product_order',
	                //'orderby' => 'meta_value_num',
	                'order' => 'ASC'
	            );

	            $loop = new WP_Query( $args );
	            $i = 1;

	            while ( $loop->have_posts() ) : $loop->the_post();
	            
	                $featured_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	                $title = get_the_title();

	                ?>
                    <li>
                        <div class="inner" style="background-image:url('<?php echo $featured_image; ?>')">
                            <div class="title">
                                <div class="border">
                                    <h3><?php echo $title; ?></h3>
                                </div>
                            </div>
                        </div>
                    </li>
	                
	                <?php

	            endwhile;

	            ?>

	    </ul><!-- end ms_surfaces -->

		<ul class="ms_surfaces_mob kwicks kwicks-vertical clearfix visible-xs">

	        	<?php
	            $args = array(
	                'post_type' => 'surface',
	                'posts_per_page' => -1,
	                //'meta_key' => 'product_order',
	                //'orderby' => 'meta_value_num',
	                'order' => 'ASC'
	            );

	            $loop = new WP_Query( $args );
	            $i = 1;

	            while ( $loop->have_posts() ) : $loop->the_post();
	            
	                $featured_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	                $title = get_the_title();

	                ?>
                    <li>
                        <div class="inner" style="background-image:url('<?php echo $featured_image; ?>')">
                            <div class="title">
                                <div class="border">
                                    <h3><?php echo $title; ?></h3>
                                </div>
                            </div>
                        </div>
                    </li>
	                
	                <?php

	            endwhile;

	            ?>

	    </ul><!-- end ms_surfaces -->

	</div><!-- end surfaces-wrapper -->

	<script type="text/javascript">

		jQuery(document).ready(function() {
            jQuery('.ms_surfaces').kwicks({
                maxSize : 300,
                spacing : 0,
                behavior: 'menu'
            });
            jQuery('.ms_surfaces_mob').kwicks({
                maxSize:150,
                behavior:'menu',
                spacing:0,
                isVertical:true
            });
		});

	</script>

	<?php $myvariable = ob_get_clean();
	return $myvariable;
}

add_action( 'vc_before_init', 'pp_vc_surfaces_main' );


function pp_vc_surfaces_main() {
	
	vc_map( array(
		'name' => __( 'Surfaces', 'pink-panda' ),
		'base' => 'pp_surfaces',
		'icon' => vc_icon(),
		"show_settings_on_create" => false,
		'category' => __( 'ms', 'pink-panda' ),
		'description' => __( 'Surfaces created by Pink Panda', 'pink-panda' ),
	));
}