<?php

// Pink Panda Button
add_shortcode( 'pp_button', 'pp_button_main' );
function pp_button_main($atts) {

	extract(shortcode_atts(array(
		'btn_title' => 'Read More',
		'btn_link' => '#',
		'btn_center' => ''
	), $atts));

	ob_start();
	$btn_center = $btn_center != false ? 'center' : '';
	?>

	<!--<div class="btn_wrapper <?php echo $btn_center ?>">
		<button class="btn_main">
			<a href="<?php echo $btn_link; ?>"><?php echo $btn_title; ?></a>
		</button>
	</div>--><!-- end btn_wrapper -->

	<a href="<?php echo $btn_link; ?>" class="btn_secondary">
        <?php echo $btn_title; ?>
        <span class="hover-bg"></span>
        <span class="stick" data-type="line" data-direction="right"></span>
        <span class="stick" data-type="line" data-direction="left"></span>
    </a>

	<?php $myvariable = ob_get_clean();
	return $myvariable;
}

add_action( 'vc_before_init', 'pp_vc_button_main' );



function pp_vc_button_main() {
	
	vc_map( array(
		'name' => __( 'Button', 'pink-panda' ),
		'base' => 'pp_button',
		'icon' => vc_icon(),
		'category' => __( 'Mirostone', 'pink-panda' ),
		'description' => __( 'Button created by Pink Panda', 'pink-panda' ),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Button Title', 'pink-panda' ),
				'param_name' => 'btn_title',
				'holder' => 'hidden',
				"admin_label" => true
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Button URL', 'pink-panda' ),
				'param_name' => 'btn_link',
				'holder' => 'hidden',
				"admin_label" => true
			),
			array(
				'type' => 'checkbox',
				'heading' => __( 'Center button', 'pink-panda' ),
				'param_name' => 'btn_center',
				'holder' => 'hidden',
			)
		),
	));
}