<?php

// Pink Panda Button
add_shortcode( 'pp_divider', 'pp_divider_main' );
function pp_divider_main($atts) {

	extract(shortcode_atts(array(
		'btn_title' => 'Read More',
		'btn_link' => '#',
		'is_elastic' => ''
	), $atts));

	ob_start();

	?>

	<div class="divider-1"></div>

	<?php $myvariable = ob_get_clean();
	return $myvariable;
}

add_action( 'vc_before_init', 'pp_vc_divider_main' );


function pp_vc_divider_main() {
	
	vc_map( array(
		'name' => __( 'Divider', 'pink-panda' ),
		'base' => 'pp_divider',
		'icon' => vc_icon(),
		"show_settings_on_create" => false,
		'category' => __( 'Mirostone', 'pink-panda' ),
		'description' => __( 'Divider created by Pink Panda', 'pink-panda' ),
		'params' => array(
			// array(
			// 	'type' => 'textfield',
			// 	'heading' => __( 'Button Title', 'pink-panda' ),
			// 	'param_name' => 'btn_title',
			// 	'holder' => 'div',
			// ),
			// array(
			// 	'type' => 'textfield',
			// 	'heading' => __( 'Button URL', 'pink-panda' ),
			// 	'param_name' => 'btn_link',
			// 	'holder' => 'div',
			// )
		),
	));
}