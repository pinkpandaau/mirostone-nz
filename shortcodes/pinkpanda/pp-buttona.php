<?php

// ! File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Pink Panda Button
add_shortcode( 'pp_button', 'pp_button_main' );
function pp_button_main($atts) {

	extract(shortcode_atts(array(
		'btn_title' => 'Read More',
		'btn_link' => '#',
		'is_elastic' => ''
	), $atts));

	ob_start();
	$is_elastic = $is_elastic != false ? 'elastic-button' : '';
	?>

	<button class="btn_main long fixed <?php echo $is_elastic ?>">
		<a href="<?php echo $btn_link; ?>"><?php echo $btn_title; ?></a>
	</button>

	<?php $myvariable = ob_get_clean();
	return $myvariable;
}

add_action( 'vc_before_init', 'pp_vc_button_main' );



function pp_vc_button_main() {
	
	vc_map( array(
		'name' => __( 'Button', 'pink-panda' ),
		'base' => 'pp_button',
		'icon' => vc_icon(),
		'category' => __( 'Pink Panda', 'pink-panda' ),
		'description' => __( 'Button created by Pink Panda', 'pink-panda' ),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Button Title', 'pink-panda' ),
				'param_name' => 'btn_title',
				'holder' => 'div',
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Button URL', 'pink-panda' ),
				'param_name' => 'btn_link',
				'holder' => 'div',
			),
			array(
				'type' => 'checkbox',
				'heading' => __( 'Elastic button?', 'pink-panda' ),
				'param_name' => 'is_elastic',
				'description' => 'force width with padding left & right'
			),
		),
	));
}