<?php

// Pink Panda Button
add_shortcode( 'pp_client_tiles', 'pp_client_tiles_main' );
function pp_client_tiles_main($atts) {

	extract(shortcode_atts(array(
		'number_videos' => '',
		'add_spacing' => ''
	), $atts));

	ob_start();

	$add_spacing = $add_spacing != false ? 'with-spacing' : '';

	?>

	<div class="client-tiles-wrapper row <?php echo $add_spacing; ?>">

		<!-- PRINT ALL OF THE CLIENT TILES FROM CLIENTS CPT
		GENERAL FUNCTIONALITY
		- ADD LOGIC, IF HOMEPAGE, ONLY GET CLIENTS WHICH BELONG TO CATEGORY 'FEATURE'
		- TILE IMAGE WILL PULL FROM FEATURED IMAGE
		- ON HOVER, TILE WILL HAVE OVERLAY, DISPLAY A PLAY BUTTON IN MIDDLE, HAVE THE 'CLIENT NAME' ABOVE OR BELOW THE PLAY BUTTON
		- UPON CLICK, MODAL BOX WILL DISPLAY WITH VIMEO EMBED
		-->

		<?php

			if(!is_numeric($number_videos)) {
				$number_videos = -1;
			}

			// IF HOMEPAGE SHOW ONLY FEATURE POSTS
			if(is_front_page()) {
				$args = array(
					'post_type' => 'client',
					'posts_per_page' => $number_videos,
					'tax_query' => array(
						array(
							'taxonomy' => 'client_category',
							'field' => 'slug',
							'terms' => 'feature'
						)
					),
					'order' => 'ASC'
				);
			} else {
				// ELSE SHOW ALL POSTS
				$args = array(
					'post_type' => 'client',
					'posts_per_page' => $number_videos,
					'order' => 'ASC'
				);
			}


            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
			$featured_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
			?>

			<div class="client-image col-md-3 col-sm-6 col-xs-12">
				<a href="<?php the_field('vimeo_url'); ?>" class="sf-video-modal image-link" style="background-image: url('<?php echo $featured_image; ?>');"></a>

				<div class="overlay">
					<a href="<?php the_field('vimeo_url'); ?>" class="icon sf-video-modal">
						<i class="fa fa-play"></i>
						<span><?php echo the_title(); ?></span>
					</a>
				</div>
			</div>
			<?php endwhile; ?>

	</div>

	<?php $myvariable = ob_get_clean();
	return $myvariable;
}

add_action( 'vc_before_init', 'pp_vc_client_tiles_main' );


function pp_vc_client_tiles_main() {
	
	vc_map( array(
		'name' => __( 'Client Tiles', 'pink-panda' ),
		'base' => 'pp_client_tiles',
		'icon' => vc_icon(),
		"show_settings_on_create" => true,
		'category' => __( 'Mirostone', 'pink-panda' ),
		'description' => __( 'Client tiles created by Pink Panda', 'pink-panda' ),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Number of videos', 'pink-panda' ),
				'param_name' => 'number_videos',
				'holder' => 'hidden',
				'admin_label' => true
			),
			array(
				'type' => 'checkbox',
				'heading' => __( 'Add spacing', 'pink-panda' ),
				'param_name' => 'add_spacing',
				'description' => 'This adds spacing between tiles',
				'admin_label' => true
			),
			// array(
			// 	'type' => 'textfield',
			// 	'heading' => __( 'Button URL', 'pink-panda' ),
			// 	'param_name' => 'btn_link',
			// 	'holder' => 'div',
			// )
		),
	));
}
