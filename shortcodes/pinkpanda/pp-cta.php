<?php

// Pink Panda Button
add_shortcode( 'pp_cta', 'pp_cta_main' );
function pp_cta_main($atts, $content = "") {

	extract(shortcode_atts(array(
		'btn_title' => 'Contct Us',
		'btn_link' => '/contact',
		'show_border' => '',
		'background_image' => '',
		'colour_scheme' => ''
	), $atts));

	$background_image = wp_get_attachment_image_src($background_image,'full');
	$background_image_url = $background_image[0];

	$cta_text = wpautop($content);


	ob_start();
	$show_border = $show_border != false ? 'border' : '';
	?>

	<div id="cta_wrapper" class="cta_wrapper <?php echo $colour_scheme; ?>">

		<div class="container-fluid" >
			<div class="bg-para" style="background-image: url('<?php echo $background_image_url; ?>'" data-start="transform:translate3d(0px, -40%, 0px)" data-end="transform:translate3d(0px, 0%, 0px)" data-anchor-target="#cta_wrapper"></div>
			<div class="cta_overlay"></div>
			<div class="cta_content <?php echo $show_border; ?>">
				
				<p><?php echo $cta_text; ?></p>
				<div class="btn_wrapper">
					<a href="<?php echo $btn_link; ?>" class="btn_secondary">
				        <?php echo $btn_title; ?>
				        <span class="hover-bg"></span>
				        <span class="stick" data-type="line" data-direction="right"></span>
				        <span class="stick" data-type="line" data-direction="left"></span>
				    </a>
				</div><!-- end btn_wrapper -->

			</div>

		</div><!-- end container -->

	</div><!-- end cta_wrapper -->

	<?php $myvariable = ob_get_clean();
	return $myvariable;
}

add_action( 'vc_before_init', 'pp_vc_cta_main' );



function pp_vc_cta_main() {
	
	vc_map( array(
		'name' => __( 'CTA', 'pink-panda' ),
		'base' => 'pp_cta',
		'icon' => vc_icon(),
		"content_element" => true,
		'category' => __( 'Mirostone', 'pink-panda' ),
		'description' => __( 'Call-To-Action created by Pink Panda', 'pink-panda' ),
		'params' => array(
			array(
	            "type" => "textarea_html",
	            "heading" => __("CTA Text", "pink-panda"),
	            "param_name" => "content",
	            "admin_label" => true,
	            "description" => __("Please enter the text which should display", "pink-panda")
	        ),
	        array(
				'type' => 'textfield',
				'heading' => __( 'Button Title', 'pink-panda' ),
				'param_name' => 'btn_title',
				'holder' => 'hidden',
				"admin_label" => true
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Button URL', 'pink-panda' ),
				'param_name' => 'btn_link',
				"admin_label" => true,
				'holder' => 'div'
			),
	        array(
	            'type' => 'attach_image',
	            'heading' => __( 'Background Image', 'pink-panda' ),
	            'param_name' => 'background_image',
	            'value' => '',
	        ),
			array(
	            'type' => 'dropdown',
	            'heading' => __( 'Colour Scheme', 'pink-panda' ),
	            'param_name' => 'colour_scheme',
	            'value' => array(
	            	'Light Grey' => 'grey',
	            	'White' => 'white',
	            	'Black' => 'black'
            	)
	        ),
	        array(
				'type' => 'checkbox',
				'heading' => __( 'Show border', 'pink-panda' ),
				'param_name' => 'show_border',
				'description' => 'Show border around CTA'
			),
		),
	));
}