<?php

// Pink Panda Button
add_shortcode( 'pp_contactinfo', 'pp_contactinfo_main' );
function pp_contactinfo_main($atts) {

	extract(shortcode_atts(array(
		'btn_title' => 'Read More',
		'btn_link' => '#',
		'is_elastic' => ''
	), $atts));

	ob_start();

	?>

	<?php global $pp_options; ?>

	<?php if($pp_options['contact-info-email'] != null || $pp_options['contact-info-phone'] != null) { ?>

		<div class="contactinfo_wrapper">

			<?php if($pp_options['contact-info-phone'] != null) {?>
				<p class="phone"><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:<?php echo $pp_options['contact-info-phone']; ?>"><?php echo $pp_options['contact-info-phone']; ?></a></p>
			<?php } ?>

			<?php if($pp_options['contact-info-email'] != null) {?>
				<p class="email"><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:<?php echo $pp_options['contact-info-email']; ?>"><?php echo $pp_options['contact-info-email']; ?></a></p>
			<?php } ?>

		</div><!-- end contactinfo_wrapper -->

	<?php } ?>

	<?php $myvariable = ob_get_clean();
	return $myvariable;
}

add_action( 'vc_before_init', 'pp_vc_contactinfo_main' );


function pp_vc_contactinfo_main() {
	
	vc_map( array(
		'name' => __( 'Confact Info', 'pink-panda' ),
		'base' => 'pp_contactinfo',
		'icon' => vc_icon(),
		"show_settings_on_create" => false,
		'category' => __( 'Mirostone', 'pink-panda' ),
		'description' => __( 'Contact Info created by Pink Panda', 'pink-panda' ),
		'params' => array(
			// array(
			// 	'type' => 'textfield',
			// 	'heading' => __( 'Button Title', 'pink-panda' ),
			// 	'param_name' => 'btn_title',
			// 	'holder' => 'div',
			// ),
			// array(
			// 	'type' => 'textfield',
			// 	'heading' => __( 'Button URL', 'pink-panda' ),
			// 	'param_name' => 'btn_link',
			// 	'holder' => 'div',
			// )
		),
	));
}