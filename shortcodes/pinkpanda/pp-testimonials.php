<?php

// Pink Panda Button
add_shortcode( 'pp_testimonials', 'pp_testimonials_main' );
function pp_testimonials_main($atts) {

	extract(shortcode_atts(array(
		'btn_title' => 'Read More',
		'btn_link' => '#',
		'is_elastic' => ''
	), $atts));

	ob_start();

	?>

	<div class="testimonials-wrapper">

		<!--<div class="testimonials-overlay"></div>-->

		<div class="ms_testimonials">

	        	<?php
	            $args = array(
	                'post_type' => 'testimonial',
	                'posts_per_page' => -1,
	                //'meta_key' => 'product_order',
	                //'orderby' => 'meta_value_num',
	                'order' => 'ASC'
	            );

	            $loop = new WP_Query( $args );
	            $i = 1;

	            while ( $loop->have_posts() ) : $loop->the_post();
	            
	                $featured_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	                $title = get_the_title();
	                $testimonial = get_field("testimonial_text");

	                ?>

	                <div class="testimonial">

	                	<div class="col-xs-12 col-sm-4 col-sm-offset-0 col-md-3 test-image">

			                <?php
				                if ($featured_image) {

				                    ?>

				                    <div class="testimonial-image" style="background-image: url('<?php echo $featured_image; ?>');">

				                        <!--<img src="<?php echo $featured_image; ?>" />-->

				                    </div><!-- end logo-wrap -->

				                    <?php
				                }
			                ?>
							<div class="test-name visible-xs">
		                		<span><?php echo $title; ?></span>
	                		</div>

		                </div>

		                <div class="col-xs-12 col-sm-8 col-md-9 testimonial-v-title">

		                	<div class="test-name hidden-xs">
		                		<span><?php echo $title; ?></span>
	                		</div>

							
							<p class="testimonial-text">
								<i class="fa fa-quote-left" aria-hidden="true"></i>
								<?php echo strip_tags($testimonial); ?>
								<i class="fa fa-quote-right" aria-hidden="true"></i>
							</p> <!-- end testimonial-text -->

							
	                	</div>

	                </div><!-- end testimonial -->

	                <?php

	            endwhile;

	            ?>

	    </div><!-- end ms_testimonials -->


		<div class="svg-wrap">
			<svg width="64" height="64" viewBox="0 0 64 64">
				<path id="arrow-left-1" d="M46.077 55.738c0.858 0.867 0.858 2.266 0 3.133s-2.243 0.867-3.101 0l-25.056-25.302c-0.858-0.867-0.858-2.269 0-3.133l25.056-25.306c0.858-0.867 2.243-0.867 3.101 0s0.858 2.266 0 3.133l-22.848 23.738 22.848 23.738z" />
			</svg>
			<svg width="64" height="64" viewBox="0 0 64 64">
				<path id="arrow-right-1" d="M17.919 55.738c-0.858 0.867-0.858 2.266 0 3.133s2.243 0.867 3.101 0l25.056-25.302c0.858-0.867 0.858-2.269 0-3.133l-25.056-25.306c-0.858-0.867-2.243-0.867-3.101 0s-0.858 2.266 0 3.133l22.848 23.738-22.848 23.738z" />
			</svg>
		</div>

	</div><!-- end testimonials-wrapper -->

	<script type="text/javascript">

		jQuery(document).ready(function(){

            var $testimonials = jQuery('.ms_testimonials');

            $testimonials.slick({
				dots: false,
				infinite: true,
				speed: 600,
				slidesToShow: 1,
				autoplay: false,
  				autoplaySpeed: 3000,
                arrows: true,
				prevArrow: '<span class="dummy-prev-arrow"></span>',
                nextArrow: '<span class="dummy-next-arrow"></span>'
			});

			$testimonials.closest( ".vc_row" ).append('<nav class="nav-diamond"><a class="prev"><span class="icon-wrap"><svg class="icon" width="28" height="28" viewBox="0 0 64 64"><use xlink:href="#arrow-left-1"></svg></span><div></div></a><a class="next"><span class="icon-wrap"><svg class="icon" width="28" height="28" viewBox="0 0 64 64"><use xlink:href="#arrow-right-1"></svg></span><div></div></a></nav>');

			jQuery('.nav-diamond .prev').click(function() {
				jQuery('.dummy-prev-arrow').click();
			})
			jQuery('.nav-diamond .next').click(function() {
				jQuery('.dummy-next-arrow').click();
			})

		});

	</script>

	<?php $myvariable = ob_get_clean();
	return $myvariable;
}

add_action( 'vc_before_init', 'pp_vc_testimonials_main' );


function pp_vc_testimonials_main() {
	
	vc_map( array(
		'name' => __( 'Testimonials', 'pink-panda' ),
		'base' => 'pp_testimonials',
		'icon' => vc_icon(),
		"show_settings_on_create" => false,
		'category' => __( 'Mirostone', 'pink-panda' ),
		'description' => __( 'Testimonials created by Pink Panda', 'pink-panda' ),
		'params' => array(
			// array(
			// 	'type' => 'textfield',
			// 	'heading' => __( 'Button Title', 'pink-panda' ),
			// 	'param_name' => 'btn_title',
			// 	'holder' => 'div',
			// ),
			// array(
			// 	'type' => 'textfield',
			// 	'heading' => __( 'Button URL', 'pink-panda' ),
			// 	'param_name' => 'btn_link',
			// 	'holder' => 'div',
			// )
		),
	));
}