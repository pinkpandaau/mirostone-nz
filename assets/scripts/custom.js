jQuery(document).ready(function() {

    // vimeo video modal
    jQuery(".sf-video-modal").YouTubePopUp( { autoplay: 1 } );

    // Columns with same height
    jQuery('.col-twins').matchHeight({
        byRow: false,
        property: 'height',
        target: null,
        remove: false
    });

    jQuery('.search-close').click(function() {
        jQuery('.search-box').removeClass('active');
    });

    jQuery('.search-toggle-icon .fa-search').click(function() {
        jQuery('.search-box').addClass('active');
        jQuery( ".search-box input" ).focus();
    });

    jQuery('.overlay-shadow').on('click',function(e){
        e.preventDefault();
        jQuery('body').toggleClass('nav-expanded');
    });

    // amorini slide
    jQuery('.toggle-amorini').on('click',function(e){
        e.preventDefault();
        jQuery('body').toggleClass('display-amorini');
    });


    // parallax effect
    jQuery('.ms_slide').each(function(){
        var $bgobj = jQuery(this);
        jQuery(window).scroll(function(){
            var yPos = (jQuery(window).scrollTop() / $bgobj.data('speed'));
            var coords = '50% ' + yPos + 'px';
            $bgobj.css({backgroundPosition: coords});
        }); 
    });

    // scroll to top arrow
    jQuery(window).scroll(function() {
        if(jQuery(this).scrollTop() > 200){
            jQuery('#slide-top').fadeIn();
        }else{
            jQuery('#slide-top').fadeOut();
        }
    });

    jQuery('#slide-top').click(function(){
       jQuery('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    // smooth scroll to anchors
    jQuery('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                if(jQuery(window).width() >= '991') {
                     jQuery('html, body').animate({
                        scrollTop: target.offset().top - 110
                    }, 1000);
                } else if(jQuery(window).width() >= '768' && jQuery(window).width() < '991'){
                     jQuery('html, body').animate({
                        scrollTop: target.offset().top - 110
                    }, 1000);
                } else if(jQuery(window).width() >= '465' && jQuery(window).width() < '768'){
                     jQuery('html, body').animate({
                        scrollTop: target.offset().top - 114
                    }, 1000);
                } else if(jQuery(window).width() <= '464'){
                     jQuery('html, body').animate({
                        scrollTop: target.offset().top - 133
                    }, 1000);
                } else {
                    jQuery('html, body').animate({
                        scrollTop: target.offset().top - 141
                    }, 1000);
                }
               
                return false;
            }
        }
    });
    
    // Sticky header
    // jQuery(window).scroll(function() {
    //     if (jQuery(this).scrollTop() > 80  && jQuery(window).width() >= '481') {  
    //             jQuery('#masthead').addClass('sticky');  
    //     } else if (jQuery(this).scrollTop() < 80  && jQuery(window).width() >= '481') {  
    //             jQuery('#masthead').removeClass('sticky');
    //     } 
    // });

    // jQuery( window ).resize(function() {     
    //     if (jQuery(this).scrollTop() > 80){  
    //         if(jQuery(window).width() >= '481') {
    //             jQuery('#masthead').addClass('sticky');   
    //         }
    //     } else {
    //         if(jQuery(window).width() >= '481') {
    //             jQuery('#masthead').removeClass('sticky');
    //         }
    //     }  
    // });
});