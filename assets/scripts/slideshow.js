jQuery(document).ready(function () {


    function slideAnimation(elem) {
        var animEndEv = 'webkitAnimationEnd animationend';
        elem.each(function () {
            var $this = jQuery(this),
                $animationType = $this.data('animation');

            $this.css('opacity', '1').addClass($animationType).one(animEndEv, function () {
                $this.removeClass($animationType);
            });
        });
    }


    var $slickElement = jQuery('.ms_slideshow');
    var $current;
    var $total;
    var $status;
    var $sliderContent = jQuery('.ms_slide');
    var $firstSlideAnimation = $slickElement.find('.ms_slide:first').find("[data-animation ^= 'animated']");


    $slickElement.slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        autoplay: false,
        arrows: true,
        fade: true,
        prevArrow: '<div class="slick-prev-past"><i class="fa fa-angle-left" aria-hidden="true"></i><div class="pagingInfo"><span class="slide-current">1</span><div class="line"></div><span class="slide-total"></span></div></div>',
        nextArrow: '<div class="slick-next-past"><i class="fa fa-angle-right" aria-hidden="true"></i><div class="pagingInfo"><span class="slide-current">1</span><div class="line"></div><span class="slide-total"></span></div></div>',
        responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                adaptiveHeight: true
            }
        }]
    });

    slideAnimation($firstSlideAnimation);

    $current = jQuery('.slide-current');
    $total = jQuery('.slide-total');
    $total.text(jQuery('.slick-slide:not(.slick-cloned)').length);

    setTimeout(function () {
        skrollr.get().refresh();
    }, 1000);

    // setTimeout(function() {
    $slickElement.on('init', function (event, slick, currentSlide, nextSlide) {
        console.log(currentSlide);
    });

    $slickElement.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var slide = jQuery(slick.$slides.get(currentSlide));
        var $caption = slide.find("[data-animation ^= 'animated']").css('opacity', '0');
    });

    $slickElement.on('afterChange', function (event, slick, currentSlide, nextSlide) {
        var i = (currentSlide ? currentSlide : 0) + 1;
        $current.text(i);
        $total.text(slick.slideCount);

        var slide = jQuery(slick.$slides.get(currentSlide));
        var $caption = slide.find("[data-animation ^= 'animated']");
        slideAnimation($caption);

    });

});