<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap" role="document">
      <div class="content">

        <!-- PRINT NEWS HEADER IF BLOG PAGE -->
        <?php if ( is_home() ) { ?>
          <header class="row news-header text-center <?php if(!the_field('featured_news_image', get_option('page_for_posts'))) echo 'no-image'; ?>" style="background-image: url('<?php the_field('featured_news_image', get_option('page_for_posts')); ?>');" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;"> 
            <div class="header-overlay">
              <h1 class="entry-title"><?php the_field('featured_news_title', get_option('page_for_posts')); ?></h1>
              <span class="breadcrumbs">
                <a href="<?php echo site_url(); ?>"><span>HOME</span></a> / <span>BLOG</span>
              </span>
            </div>
          </header>
        <?php } ?>

       

        <div class="container">
          <main class="col-md-8">
            <?php include Wrapper\template_path(); ?>
          </main><!-- /.main -->
          <?php if (Setup\display_sidebar()) : ?>
            <aside class="col-md-4 sidebar">
              <?php include Wrapper\sidebar_path(); ?>
            </aside><!-- /.sidebar -->
          <?php endif; ?>
        </div><!-- end container -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
      include_once("analyticstracking.php");
    ?>
  </body>
</html>
