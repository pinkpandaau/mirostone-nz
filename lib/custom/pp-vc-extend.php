<?php

// ! File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Removed unused VC elements
if (function_exists('vc_remove_element')) {
	vc_remove_element( "vc_message" );
	vc_remove_element( "vc_facebook" );
	vc_remove_element( "vc_tweetmeme" );
	vc_remove_element( "vc_googleplus" );
	vc_remove_element( "vc_separator" );
	vc_remove_element( "vc_pinterest" );
	vc_remove_element( "vc_toggle" );
	vc_remove_element( "vc_images_carousel" );
	vc_remove_element( "vc_tta_tabs" );
	vc_remove_element( "vc_tta_tour" );
	vc_remove_element( "vc_tta_accordion" );
	vc_remove_element( "vc_tta_pageable" );
	vc_remove_element( "vc_posts_slider" );
	vc_remove_element( "vc_widget_sidebar" );
	//vc_remove_element( "vc_raw_html" );
	vc_remove_element( "vc_raw_js" );
	vc_remove_element( "vc_flickr" );
	vc_remove_element( "vc_progress_bar" );
	vc_remove_element( "vc_pie" );
	vc_remove_element( "vc_round_chart" );
	vc_remove_element( "vc_line_chart" );
	vc_remove_element( "vc_custom_heading" );
	vc_remove_element( "vc_cta" );
	vc_remove_element( "vc_basic_grid" );
	vc_remove_element( "vc_media_grid" );
	vc_remove_element( "vc_masonry_grid" );
	vc_remove_element( "vc_masonry_media_grid" );
	vc_remove_element( "vc_wp_search" );
	vc_remove_element( "vc_wp_meta" );
	vc_remove_element( "vc_wp_recentcomments" );
	vc_remove_element( "vc_wp_calendar" );
	vc_remove_element( "vc_wp_pages" );
	vc_remove_element( "vc_wp_tagcloud" );
	vc_remove_element( "vc_wp_custommenu" );
	vc_remove_element( "vc_wp_text" );
	vc_remove_element( "vc_wp_posts" );
	vc_remove_element( "vc_wp_categories" );
	vc_remove_element( "vc_wp_archives" );
	vc_remove_element( "vc_wp_rss" );
	vc_remove_element( "vc_text_separator" );
}

// Remove deprecated elements
if (function_exists('vc_remove_element')) {
	vc_remove_element( "vc_tabs" );
	vc_remove_element( "vc_tour" );
	vc_remove_element( "vc_accordion" );
	vc_remove_element( "vc_button" );
	vc_remove_element( "vc_button2" );
	vc_remove_element( "vc_cta_button" );
	vc_remove_element( "vc_cta_button2" );
}

// Returns VC icon
function vc_icon($name = 'pp-vc.png') {
	return get_template_directory_uri() . "/assets/images/admin/shortcodes/vc/" . $name;
}