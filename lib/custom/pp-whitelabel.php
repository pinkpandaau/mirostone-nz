<?php

// Disable WordPress Admin Bar for all users but admins
show_admin_bar(false);

// Site by Pink Panda in WP Dashboard
function pinkpanda_dashboard_attribution($footer) {
	echo 'Site by <a href="http://www.pinkpanda.com.au">Pink Panda Digital Solutions</a>';
}

add_filter('admin_footer_text', 'pinkpanda_dashboard_attribution');

// Remove WP version for non-admins
function pinkpanda_remove_wp_version() {
    if (!current_user_can('manage_options') ) {
        remove_filter( 'update_footer', 'core_update_footer' ); 
    }
}
add_action( 'admin_menu', 'pinkpanda_remove_wp_version' );

// Remove widgets from dasboard
function pinkpanda_dashboard_cleanup() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
} 
add_action('wp_dashboard_setup', 'pinkpanda_dashboard_cleanup' );

// Enqueue admin.css
function load_custom_wp_admin_style() {
    global $themes_version;
    wp_enqueue_style( 'custom_wp_admin_css', get_template_directory_uri() . '/assets/styles/admin.css', false, $themes_version  );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


// Customize Login Screen
function pinkpanda_login_screen() {

    ?>

    <style type="text/css">

        body:before {
             background: url('<?php echo get_bloginfo('template_directory'); ?>/assets/images/admin/pink-panda-logo.png');
             background-size: 100%;
             background-repeat: no-repeat;
             content: '';
             width: 160px;
             height: 100px;
             position: absolute;
             bottom: 0px;
             left: 0px;
             right: 0px;
             margin-left: auto;
             margin-right: auto;
        }

        body {
            position: relative;
        }

        input {
            box-shadow: none !important;
            border: 1px solid #e0e0e0 !important;
        }

        input[type="submit"] {
            /*font-family: "Lato" !important;*/
            background-color: rgb(246,0,100) !important;
            /*padding: 5px 15px !important;
            height: 35px !important;*/
            border-radius: 0px !important;
        }

        input[type="submit"]:hover {
            background-color: rgb(230, 45, 120) !important;
        }

        .wp-core-ui .button-primary {
            text-shadow: none !important;
        }

        .login h1 a {
            background: url('<?php echo get_bloginfo('template_directory'); ?>/assets/images/admin/logo.png') no-repeat center !important;
            background-size: 80% !important;
            width: 70% !important;
            background-size: contain !important;
            /*height: 150px;&/
        }

        .login {
            /*background-image: url('<?php echo get_bloginfo('template_directory'); ?>/assets/images/admin/admin-texture.jpg');*/
            background-repeat: repeat;
            /*background-color: #f45;*/
        }

        @media (min-width: 2000px) {
            #login {
                padding: 6% 0 0;
            }

        }

    </style>

<?php }
add_action('login_head', 'pinkpanda_login_screen');

// Remove 'Forgot Password' link
function pinkpanda_remove_lost_password ( $text ) {
	 if ($text == 'Lost your password?'){$text = '';}
		return $text;
	 }
add_filter( 'gettext', 'pinkpanda_remove_lost_password' );



// Force login with email address only
remove_filter('authenticate', 'wp_authenticate_username_password', 20);
add_filter('authenticate', 'pinkpanda_force_email_login', 20, 3);
function pinkpanda_force_email_login($user, $email, $password){

    //Check for empty fields
    if(empty($email) || empty ($password)){        
        //create new error object and add errors to it.
        $error = new WP_Error();

        if(empty($email)){ //No email
            $error->add('empty_username', __('<strong>ERROR</strong>: Email field is empty.'));
        }
        else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //Invalid Email
            $error->add('invalid_username', __('<strong>ERROR</strong>: Email is invalid.'));
        }

        if(empty($password)){ //No password
            $error->add('empty_password', __('<strong>ERROR</strong>: Password field is empty.'));
        }

        return $error;
    }

    //Check if user exists in WordPress database
    $user = get_user_by('email', $email);

    //bad email
    if(!$user){
        $error = new WP_Error();
        $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
        return $error;
    }
    else{ //check password
        if(!wp_check_password($password, $user->user_pass, $user->ID)){ //bad password
            $error = new WP_Error();
            $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
            return $error;
        }else{
            return $user; //passed
        }
    }
}

// Change text at login screen to remove 'Username'
add_filter('gettext', 'pinkpanda_change_login_text', 20);
function pinkpanda_change_login_text($text){
    if(in_array($GLOBALS['pagenow'], array('wp-login.php'))){
        if('Username' == $text){
            return 'Email Address';
        }
				if('Username or Email'== $text){
            return 'Email Address';
        }
    }
    return $text;
}



function load_zendesk_widget() {
        echo '<!-- Start of pinkpandaau Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src=\'javascript:var d=document.open();d.domain="\'+n+\'";void(0);\',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write(\'<body onload="document._l();">\'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","pinkpandaau.zendesk.com");
/*]]>*/</script>
<!-- End of pinkpandaau Zendesk Widget script -->';
}
add_action( 'admin_head', 'load_zendesk_widget' );



function removeDemoModeLink() { // Be sure to rename this function to something more unique
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
    }
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );    
    }
}
add_action('init', 'removeDemoModeLink');

/** remove redux menu under the tools **/
add_action( 'admin_menu', 'remove_redux_menu',12 );
function remove_redux_menu() {
    remove_submenu_page('tools.php','redux-about');
}