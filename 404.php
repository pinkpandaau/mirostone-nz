<?php get_template_part('templates/page', 'header'); ?>

<div class="page-header"> 
  <div class="page-title">
  <h2 class="text-xs-center">404 - Page Error - Page Not Found</h2>
  </div>
</div>

<div class="results">
  <p>This page / file may have been moved or deleted.</p>
  <p>Please go back to <a href="<?php echo get_site_url();?>" class="return-text">Home</a> or pages from navigation above.</p>
</div>
