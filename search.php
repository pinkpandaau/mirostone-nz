<?php get_template_part('templates/page', 'header'); ?>

<h2 class="pagetitle">
   Results for
    <?php $key = wp_specialchars($s, 1); ?>
    <span class="search-terms">
      <?php echo '"'.$key.'"'; ?>
    </span> 
</h2>

<?php if (!have_posts()) : ?>
  <div class="results">
    <?php _e('No results found.', 'sage'); ?>
  </div>
  <?php //get_search_form(); ?>
<?php endif; ?>


<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'search'); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?>
