<?php
/**
 * Template Name: Container Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<div class="container main_container">
		<?php //get_template_part('templates/page', 'header'); ?>
		<?php get_template_part('templates/content', 'page'); ?>
	</div><!-- end contrainer -->
<?php endwhile; ?>

<script>
jQuery(function() {
    jQuery('.row-eq-height-1').matchHeight({
	    byRow: true,
	    property: 'height',
	    target: null
	});
});
</script>